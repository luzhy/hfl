{-# LANGUAGE NamedFieldPuns #-}
module Main where

import Data.List 
import Data.Maybe
import Data.List.Split
import qualified Data.Map as Map
import Control.Monad.IO.Class
import System.Environment
import System.Console.Haskeline
import System.Console.Haskeline.Completion
import Debug.Trace

import Logic.HFL

data TmpEnv = TmpEnv { iTS  :: Maybe TransitionSystem,
                       iLTS :: Maybe (LTS, Map.Map String HFLType),
                       iFmu :: Maybe MuFormula,
                       iRes :: Maybe HFLType} deriving (Show)

defaultKeywordList = sort ["printFormu", "printLTS", "printRes", "readLTSFile", "parseFormuStr", "typeCheck", "modelCheck", "quit"]

searchFunc :: [String] -> String -> [Completion]
searchFunc keywordList str = map simpleCompletion $ filter (str `isPrefixOf`) keywordList

renewCompletionInEnv :: Monad m => [String] -> Settings m -> Settings m
renewCompletionInEnv newKeywordList oldEnv =
    setComplete newCompleteFunc oldEnv
    where
        newCompleteFunc = 
            completeWord Nothing " \t" $ return . searchFunc newKeywordList


--until_ :: Monad m => (a -> Bool) -> m a -> (a -> m ()) -> m ()
until_ pred prompt action = \newEnv -> do
    result <- prompt
    if pred result 
    then return ()
    else action result newEnv >>= until_ pred prompt action

evalString str newEnv = 
    case (head orderList) of
        "printFormu"    -> return (fmap show $ iFmu newEnv, newEnv)

        "printLTS"      -> return (fmap show $ iLTS newEnv, newEnv)

        "printRes"      -> return (fmap show $ iRes newEnv, newEnv)

        "readLTSFile"   -> do 
                               x <- readFile (init (concatMap (++ " ") (tail orderList)))
                               let ts  = fromJust $ parseTransitionSystem x
                               let lts = transitionSystemToGraph ts
                               let lbm = labelMapping lts ts
                               return (Just (show ts), 
                                       (newEnv {iTS  = Just ts, 
                                                iLTS = Just (lts, lbm)}))
                               
        "parseFormuStr" -> return (Just (show f), (newEnv {iFmu  = Just f})) 
                           where
                               x = (init (concatMap (++ " ") (tail orderList)))
                               f = parseForm x

        "typeCheck"     -> case tcRes of
                               Left  t -> return (Just (show t), newEnv)
                               Right _ -> return (Just "Type Error", newEnv)
                           where
                               tcRes =
                                   inferHFL Map.empty (fromJust $ iFmu newEnv)

        "modelCheck"    -> return (Just (show res), newEnv {iRes = Just res})
                           where 
                               res = naiveAlgo (fromJust (iFmu newEnv))
                                               (fst (fromJust (iLTS newEnv)))
                                               (snd (fromJust (iLTS newEnv)))
        _               -> return (Nothing, newEnv)
    where
        orderList = splitOn " " str
        
replLoop :: InputT IO ()
replLoop = do
    until_ quitPred (getInputLine "HFLIntp> ") (evalAndPrint . fromJust) 
           (TmpEnv Nothing Nothing Nothing Nothing)
    where 
        quitPred Nothing = True 
        quitPred (Just "quit") = True
        quitPred _ = False

        evalAndPrint str newEnv = do
            evalResult <- liftIO $ evalString str newEnv 
            case evalResult of
                (Nothing,  outputEnv)  -> do outputStrLn "Nothing"
                                             return outputEnv
                (Just res, outputEnv)  -> do outputStrLn res
                                             return outputEnv

runRepl :: IO ()
runRepl = do
    let myHistoryFile = Just "./.HFLCommandLineHistory"
    let myCompleteFunc = completeWord Nothing " \t" $ return.searchFunc defaultKeywordList
    let mySettings = Settings { historyFile    = myHistoryFile
                              , complete       = myCompleteFunc
                              , autoAddHistory = True }
    runInputT mySettings replLoop

main :: IO ()
main = do
    args <- getArgs
    if null args then runRepl else putStrLn "Error: unfinished feature"
