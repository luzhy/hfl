module Main(main) where

import Test.Hspec
import Test.QuickCheck

import Synt

main :: IO ()
main = hspec $ do
    simpleTests

simpleTests :: Spec
simpleTests = describe "simple test" $ do
    describe "Single Identifiler" $ do
        it "Input V1" $
            parseForm "V1" `shouldBe` (Var "V1")     


a = "(0, 9, 4)\n(0, \"a\", 0)\n(0, \"b\", 0)\n(0, \"b\", 1)\n(1, \"a\", 2)\n(1, \"b\", 2)\n(2, \"a\", 3)\n(2, \"b\", 3)\n(3, \"a\", 3)\n(3, \"b\", 3)\n(\"q\",[0,1,2])\n" 

a2 = "(0, 3, 3)\n(0, \"a\", 0)\n(1, \"a\", 1)\n(2, \"a\", 2)\n(\"q\",[0])"

exts  = fromJust $ parseTransitionSystem a
exlts = transitionSystemToGraph exts
lbmap = labelMapping exlts exts

d = LFP (MuVar "X") 
        (TypeAppl TypeProp TypeProp)
        (Lam (MuVar "Z") 
             TypeProp
             (Or (Var (MuVar "Z")) 
                 (Or (App (Var (MuVar "X")) 
                          (Box "a" (Var (MuVar "Z"))))
                     (App (Var (MuVar "X")) 
                          (Box "b" (Var (MuVar "Z")))))))

t1 = "_u(Vx : Pr + -> Pr). _L(Vz + : Pr). Vz | (Vx [a]Vz) | (Vx [b]Vz)"
t2 = "_L(Vf-:Pr+->Pr)._L(Vg+:Pr+->Pr).(_u(Vz:Pr-->Pr+->Pr)._L(Vx-:Pr)._L(Vy+:Pr).(~Vx|Vy) & Vz (Vf Vx) (Vg Vy)) tt (Vg tt)"

e = Not (Prp "q")

b1 = (Lam (MuVar "X") (TypeAppl TypeProp TypeProp) (App (Var (MuVar "X")) (Prp "q")))

b2 = (Lam (MuVar "Y") TypeProp (Not (Var (MuVar "Y"))))

b3 = (LFP (MuVar "Y") (TypeAppl TypeProp TypeProp) (Var (MuVar "Y")))

b4 = (LFP (MuVar "Y") (TypeAppl TypeProp TypeProp) (Lam (MuVar "Z") TypeProp (App (Var (MuVar "Y")) (Var (MuVar "Z")))))

b5 = (GFP (MuVar "Y") (TypeAppl TypeProp TypeProp) (Var (MuVar "Y")))

b6 = (GFP (MuVar "Y") (TypeAppl TypeProp TypeProp) (Lam (MuVar "Z") TypeProp (App (Var (MuVar "Y")) (Var (MuVar "Z")))))

b7 = (LFP (MuVar "X") (TypeAppl TypeProp TypeProp) (Lam (MuVar "Y") TypeProp (Or (Var (MuVar "Y")) (App (Var (MuVar "X")) (Box "a" (Var (MuVar "Y")))))))  


