module Logic.HFL.MuCalculus where

data SVari =
    SZero
  | SPlus
  | SMinu
  deriving (Show, Eq)

data TType =
      TPr
    | TAp TType SVari TType
    deriving (Show, Eq)

data MuVar = MuVar String
    deriving (Show, Read, Eq, Ord)

data MuFormula = 
      MuT
    | MuF
    | Prp String
    | Var MuVar
    | Not MuFormula
    | Orr MuFormula MuFormula
    | And MuFormula MuFormula
    | Box String MuFormula
    | Dmd String MuFormula
    | LFP MuVar TType MuFormula
    | GFP MuVar TType MuFormula
    | Lam MuVar SVari TType MuFormula
    | App MuFormula MuFormula
    deriving (Show, Eq)

