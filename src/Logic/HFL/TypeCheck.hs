module Logic.HFL.TypeCheck where

import qualified Data.Map as Map
import qualified Data.Maybe as Maybe
import Control.Monad.State
import Logic.HFL.MuCalculus

import Debug.Trace

type HFLTEnv = Map.Map MuVar (SVari, TType)

type Err = Either TType String

negOp hflEnv =
    Map.map (\(sv, t) -> (negSV sv, t)) hflEnv
    where
        negSV SZero = SZero
        negSV SPlus = SMinu
        negSV SMinu = SPlus

inferHFL :: HFLTEnv -> MuFormula -> Err
inferHFL hflEnv muFormu = case muFormu of
    MuT         -> Left TPr
    MuF         -> Left TPr
    Prp n       -> Left TPr

    Var v       -> 
        if Maybe.isNothing res
        then Right "Type Error!"
        else 
            if fst (Maybe.fromJust res) == SMinu 
            then Right "Type Error!"
            else Left $ snd (Maybe.fromJust res)
        where
            res = Map.lookup v hflEnv
            
    Not x       -> 
        if inferHFL (negOp hflEnv) x == Left TPr
        then Left TPr
        else Right "Type Error!"

    Orr x y     ->
        if inferHFL hflEnv x == Left TPr && inferHFL hflEnv y == Left TPr 
        then Left TPr
        else Right "Type Error!"
    And x y     ->
        if inferHFL hflEnv x == Left TPr && inferHFL hflEnv y == Left TPr 
        then Left TPr
        else Right "Type Error!"

    Box n x     ->
        if inferHFL hflEnv x == Left TPr
        then Left TPr
        else Right "Type Error!"
    Dmd n x     ->
        if inferHFL hflEnv x == Left TPr
        then Left TPr
        else Right "Type Error!"

    LFP v t x   ->
        if inferHFL (Map.insert v (SPlus, t) hflEnv) x == Left t
        then Left t
        else Right "Type Error!"
    GFP v t x   ->
        if inferHFL (Map.insert v (SPlus, t) hflEnv) x == Left t
        then Left t
        else Right "Type Error!"

    Lam v s t x ->
        Left (TAp t s tx)
        where
            Left tx = inferHFL (Map.insert v (s, t) hflEnv) x

    App x y     ->
        case iFuncT of
            TAp ta SZero tb ->
                if inferHFL hflEnv         y == Left ta && 
                   inferHFL (negOp hflEnv) y == Left ta
                then Left tb
                else Right "Type Error!"
            TAp ta SPlus tb ->
                if inferHFL hflEnv y == Left ta
                then Left tb
                else Right "Type Error!"
            TAp ta SMinu tb -> 
                if inferHFL (negOp hflEnv) y == Left ta
                then Left tb
                else Right "Type Error!"
            _               -> Right "Type Error!"
        where 
            Left iFuncT = inferHFL hflEnv x
    
