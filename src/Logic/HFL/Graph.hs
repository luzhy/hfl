module Logic.HFL.Graph where

import Data.Map.Lazy (Map)
import Data.Set (Set)
import Data.Foldable
import Data.Maybe (fromJust)
import qualified Data.Map.Lazy as Map
import qualified Data.Set as Set

import Logic.HFL.TransitionSystem

data Node a b = Node
    { state :: a
    , pre   :: Map b (Graph a b)
    }

type Graph a b = Set (Node a b)

type LTS = Graph Integer String

data HFLType = 
      HFLVar LTS
    | HFLFunc (Map HFLType HFLType)
    deriving(Show, Eq, Ord)

instance (Show a, Show b) => Show (Node a b) where
    show (Node state pre) = show state ++ " "

instance Eq a => Eq (Node a b) where
    (Node s1 _) == (Node s2 _) = s1 == s2

instance Ord a => Ord (Node a b) where
    (Node s1 _) `compare` (Node s2 _) = s1 `compare` s2

getPre :: (Ord b) => Node a b -> b -> Graph a b
getPre t l = Map.findWithDefault Set.empty l (pre t)

transitionSystemToGraph :: TransitionSystem -> Graph Integer String;
transitionSystemToGraph = mkGraph . mkPrejacencyMap . edges

mkGraph :: (Ord a) => Map a (Map b (Set a)) -> Graph a b
mkGraph links = foldMap Set.singleton nodeLookupList where
--    mkNode :: (Ord a) => a -> Map b (Set a) -> Node a b
    mkNode state pre = Node state $ fmap (Set.map lookupNode) pre
--    nodeLookupList :: (Ord a) => Map a (Node a b)
    nodeLookupList = Map.mapWithKey mkNode links
--    lookupNode :: (Ord a) => a -> Node a b
    lookupNode state = fromJust $ Map.lookup state nodeLookupList

mkPrejacencyMap :: [Edge] -> Map Integer (Map String (Set Integer));
mkPrejacencyMap = foldl addEdge Map.empty where
    addEdge map (Edge s l e) = insertedRight where
        insertedLeft = Map.insertWith addPre s Map.empty map
        insertedRight = Map.insertWith addPre e (Map.singleton l (Set.singleton s)) insertedLeft
    addPre = Map.unionWith Set.union

mkLabelling lts lb = map (\t -> (propoSition t, 
                                 HFLVar $
                                 Set.fromList $
                                     map (\i -> (Set.toList lts) !! 
                                                (fromIntegral i)) 
                                         (stateLabelList t))) lb

--labelMapping :: LTS -> TransitionSystem -> Map String LTS
labelMapping lts ts = Map.fromList $ mkLabelling lts (labelling ts)
