module Logic.HFL.TransitionSystem where

import qualified Data.Map.Lazy as Map
import Data.List.Split
import Data.List
import Data.Maybe
import Text.Read

data TransitionSystem = TransitionSystem { firstState :: Integer
                                         , transitionCount :: Integer
                                         , stateCount :: Integer
                                         , labelling :: [LabelMap]
                                         , edges :: [Edge]} deriving (Show)

data Edge = Edge { startState :: Integer
                 , label :: String
                 , endState :: Integer} deriving (Show)

data LabelMap = LabelMap { propoSition :: String
                         , stateLabelList :: [Integer]} deriving (Show)

parseTransitionSystem :: String -> Maybe TransitionSystem
parseTransitionSystem str = do
    split <- splitTuple . head . lines $ str
    [i1, i2, i3] <- mapM (maybeReadIndex split) [0, 1, 2]
    e <- mapM parseEdge (take (fromIntegral i2) $ tail . lines $ str)
    l <- mapM parseLabel (drop (fromIntegral i2) $ tail . lines $ str)
    return TransitionSystem { firstState = i1
                            , transitionCount = i2
                            , stateCount = i3
                            , labelling = l
                            , edges = e}

parseEdge str = do
    split <- splitTuple str
    [i1, i3] <- mapM (maybeReadIndex split) [0, 2]
    i2 <- maybeReadIndex split 1
    return Edge { startState = i1
                , label = i2
                , endState = i3}

parseLabel str = do
    split <- splitTuple str
    i1 <- maybeReadIndex split 0
    i2 <- readMaybe (init (concatMap (++ ",") (tail split)))
    return LabelMap { propoSition = i1
                    , stateLabelList = i2}

parseTriple str = do
    split <- splitTuple str
    mapM (maybeReadIndex split) [0, 1, 2]

splitTuple str = (\i j -> splitOn "," . drop (i + 1) . take j $  str)
                 <$> elemIndex '(' str <*> elemIndex ')' str

maybeReadIndex ls i = (listToMaybe . drop i $ ls) >>= readMaybe
