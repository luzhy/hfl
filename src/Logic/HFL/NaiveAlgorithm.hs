module Logic.HFL.NaiveAlgorithm where

import Data.Set (Set)
import Data.Map.Lazy (Map)
import Data.Maybe (fromJust)

import qualified Data.Set as Set
import qualified Data.Map.Lazy as Map
import qualified Data.List as List

import Logic.HFL.MuCalculus
import Logic.HFL.TransitionSystem
import Logic.HFL.Graph

makeDomain ari state
    | ari == 0 = [[]]
    | ari == 1 = do 
                     x <- List.subsequences state
                     return [x]
    | otherwise = do
                     x <- makeDomain (ari - 1) state
                     y <- List.subsequences state
                     return (y:x)

makeCombin l = case l of
    []    -> [[]]
    hd:tl -> do
                 x <- hd
                 y <- makeCombin tl
                 return (x:y)

makeDomain2 hfltype state = case hfltype of
    TPr         -> map (HFLVar . Set.fromList) (List.subsequences state)
    TAp t1 s t2 -> map (HFLFunc . (Map.fromList)) (makeCombin $
                     do x <- (makeDomain2 t1 state)
                        return $ do y <- (makeDomain2 t2 state)
                                    return (x,y))

makeDomain3 hfltype start state =
    head $ evalloop hfltype
    where
        evalloop htype = case htype of
            TPr         -> [HFLVar start]
            TAp t1 s t2 -> map (HFLFunc . (Map.fromList)) (makeCombin $
                             do x <- (makeDomain2 t1 state) 
                                return $ do y <- (evalloop t2)
                                            return (x,y))

naiveAlgo :: MuFormula -> LTS -> Map String HFLType -> HFLType
naiveAlgo f lts labelmap = eval f lts Map.empty []
    where
        eval f lts e param = case f of
            MuT     -> HFLVar lts
            MuF     -> HFLVar Set.empty
            Var x   -> varOp x lts e param
            Prp x   -> fromJust $ Map.lookup x labelmap
            Not x   -> notOp x lts e param
            And x y -> andOp x y lts e param
            Orr x y -> orrOp x y lts e param
            Box b y -> boxOp b y e
            Dmd b y -> diamondOp b y e 
            LFP x t f   -> fixedPoint Set.empty f e x t param
            GFP x t f   -> fixedPoint lts f e x t param
            Lam x s t f -> lamOp x s t f lts e param
            App x y -> appOp x y lts e param

        andOp x y lts e param = 
            HFLVar $ Set.intersection s1 s2
            where 
                HFLVar s1 = eval x lts e []
                HFLVar s2 = eval y lts e []

        orrOp x y lts e param = 
            HFLVar $ Set.union s1 s2
            where 
                HFLVar s1 = eval x lts e []
                HFLVar s2 = eval y lts e []

        varOp x lts e param =
            fromJust $ Map.lookup x e

        notOp x lts e param = case x of
            Var mv -> HFLVar $ Set.difference lts res
            Prp s  -> HFLVar $ Set.difference lts res
            _      -> error ""
            where 
                HFLVar res = eval x lts e param

        appOp x y lts e param =
            fromJust $ Map.lookup val fun
            where
                HFLFunc fun = eval x lts e []
                val = eval y lts e []

        lamOp x s t f lts e param =
            HFLFunc $ Map.fromList ltsdomain
            where
                ltsdomain = List.map 
                                (\l -> (l, makeRes l)) 
                                (makeDomain2 t (Set.toList lts))
                makeRes l = eval f lts (Map.insert x l e) []
               
        boxOp b y e =
            HFLVar $ foldl Set.difference lts (Set.map f lts)
            where
                f x = if Set.member x k then Set.empty else getPre x b
                k = Set.union Set.empty res
                HFLVar res = (eval y lts e [])

        diamondOp b y e =
            HFLVar $ foldl Set.union Set.empty (Set.map f lts)
            where
                f x = if Set.member x k then getPre x b  else Set.empty
                k = Set.union Set.empty res
                HFLVar res = (eval y lts e [])

        fixedPoint start f e varName t param =
            loopenv initv
            where
                initv = makeDomain3 t start (Set.toList lts)
                loopenv oldv =
                    if   oldv == newv then oldv else loopenv newv
                    where
                        newv  = eval f lts (Map.insert varName oldv e) param
                
