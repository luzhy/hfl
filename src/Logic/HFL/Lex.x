{
module Logic.HFL.Lex where
}

%wrapper "basic"

$alpha = [a-zA-Z]
$num = [0-9]

tokens :-
  $white+        ;

  "ff"           { \s -> TFalse }
  "tt"           { \s -> TTrue  }

  "Pr"           { \s -> TPrType }

  "0"            { \s -> TZero }
  "+"            { \s -> TPlus }
  "-"            { \s -> TMinu }

  "&"            { \s -> TAnd }
  "|"            { \s -> TOrr }
  "~"            { \s -> TNot }

  "_L"           { \s -> TLambda }
  "->"           { \s -> TArrow  }

  ":"            { \s -> TSemiColon }

  "."            { \s -> TDot }
  "_v"           { \s -> TGfp }
  "_u"           { \s -> TLfp }

  "<"            { \s -> TLeftABrack  }
  ">"            { \s -> TRightABrack }
  "["            { \s -> TLeftSBrack  }
  "]"            { \s -> TRightSBrack }

  "("            { \s -> TLeftParen  }
  ")"            { \s -> TRightParen }

  "V" [$alpha$num]+    { \s -> TVName s }
  "P" [$alpha$num]+    { \s -> TPName s }
  [$alpha$num]+        { \s -> TAName s }

{

data Token =
    TPName String
  | TVName String
  | TAName String

  | TFalse
  | TTrue

  | TPrType

  | TZero
  | TPlus
  | TMinu

  | TAnd
  | TOrr
  | TNot

  | TLambda
  | TArrow

  | TSemiColon

  | TDot
  | TGfp
  | TLfp

  | TLeftABrack
  | TLeftSBrack
  | TRightABrack
  | TRightSBrack

  | TLeftParen
  | TRightParen
  deriving (Eq, Show)

}
