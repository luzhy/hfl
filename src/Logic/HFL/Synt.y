{
module Logic.HFL.Synt where

import Logic.HFL.Lex
import Logic.HFL.MuCalculus
}

%name synt
%tokentype { Token      }
%error     { parseError }

%token
  pname       { TPName $$ }
  vname       { TVName $$ }
  aname       { TAName $$ }

  "ff"        { TFalse }
  "tt"        { TTrue  }

  "Pr"        { TPrType }

  "0"         { TZero }
  "+"         { TPlus }
  "-"         { TMinu }

  "&"         { TAnd }
  "|"         { TOrr }
  "~"         { TNot }

  "_L"        { TLambda }
  "->"        { TArrow  }

  ":"         { TSemiColon }

  "."         { TDot }
  "_v"        { TGfp }
  "_u"        { TLfp }

  "<"         { TLeftABrack  }
  ">"         { TRightABrack }
  "["         { TLeftSBrack  }
  "]"         { TRightSBrack }

  "("         { TLeftParen  }
  ")"         { TRightParen }

%%

MuFormula:
    "_v" "(" vname ":" TType ")" "." MuFormula 
        { GFP (MuVar $3) $5 $8      }
  | "_u" "(" vname ":" TType ")" "." MuFormula 
        { LFP (MuVar $3) $5 $8      }
  | "_L" "(" vname SVari ":" TType ")" "." MuFormula 
        { Lam (MuVar $3) $4 $6 $9   }
  | PMuFormula 
        { $1                        }

PMuFormula:
    PMuFormula PPMuFormula
        { App $1 $2                 }
  | PPMuFormula 
        { $1                        }

PPMuFormula:
    PPMuFormula "|" PPPMuFormula 
        { Orr $1 $3 }
  | PPMuFormula "&" PPPMuFormula 
        { And $1 $3 }
  | PPPMuFormula 
        { $1        }

PPPMuFormula:
    "[" aname "]" PPPMuFormula { Box $2 $4 }
  | "<" aname ">" PPPMuFormula { Dmd $2 $4 }
  | PPPPMuFormula { $1 }

PPPPMuFormula:
    "~" PPPPMuFormula { Not $2 }
  | pname { Prp $1         }
  | vname { Var (MuVar $1) }
  | "tt"  { MuT            }
  | "ff"  { MuF            }
  | "(" MuFormula ")" { $2 }

TType :
    PTType 
        { $1           }
  | PTType SVari "->" TType 
        { TAp $1 $2 $4 }

PTType :
    "Pr"          { TPr }
  | "(" TType ")" { $2  }

SVari :
    "+" { SPlus }
  | "-" { SMinu }
  | "0" { SZero }

{

parseError :: [Token] -> a
parseError _ = error "Parse error"

parseTokens = alexScanTokens

parseForm = synt . parseTokens 

}
